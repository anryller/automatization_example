pipelineJob('support-restart-exporer-worker') {
    description "DO NOT EDIT MANUALLY. Created from https://github.com/YouToken/jenkins-jobs\n Restart explorer workers on prod"

    parameters { 
    choiceParam('worker', ['bch', 'btc', 'eos', 'ethw', 'ltc', 'sgb', 'xrp', 'bnb', 'bsc', 'dash', 'eth', 'flr', 'omni', 'xlm'], 'Select worker to restart') 
    }

    definition {
        cps {
            script(readFileFromWorkspace('support/jobs/restart_exporer_worker.groovy'))
            sandbox()
        }
    }
}
