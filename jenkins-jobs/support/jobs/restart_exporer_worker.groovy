pipeline {
  agent {
    node {
      label 'AWS'
    }
  }

  options {
    timeout(time: 5, unit: 'MINUTES')
  }

  environment {
    registry = 'https://651309617045.dkr.ecr.eu-central-1.amazonaws.com'
    githubCredId = 'jenkins-to-k8s-tenant-youhodler-backend-deploy-key'
    GIT_AUTHOR_NAME = "${currentBuild.getBuildCauses()[0].userName}"
    GIT_AUTHOR_EMAIL = "jenkins-youhodler@users.noreply.github.com"
    GIT_COMMITTER_NAME = "${currentBuild.getBuildCauses()[0].userName}"
    GIT_COMMITTER_EMAIL = "jenkins-youhodler@users.noreply.github.com"
    overlayPath = 'workloads/overlays/prod'
    registryName = '651309617045.dkr.ecr.eu-central-1.amazonaws.com'
    serviceName = 'explorer'
    imageName = 'explorer'
    worker = "${params.worker}".trim()
  }

  stages {
    stage('Restart') {
      steps {
        script {
          currentBuild.displayName = "#${env.BUILD_NUMBER} ${worker}"
        }

        git url: 'git@github.com:YouToken/k8s-tenant-youhodler-backend.git',
            branch: 'master',
            credentialsId: githubCredId

        dir("${overlayPath}/${serviceName}/patches/workers") {
          script {
            withCredentials([sshUserPrivateKey(credentialsId: githubCredId, keyFileVariable: 'SSH_KEY')]) {
              withEnv(["GIT_SSH_COMMAND=ssh -o StrictHostKeyChecking=no -i ${SSH_KEY}"]) {
                docker.image('mikefarah/yq:4').inside('--entrypoint=""') {
                  sh 'CURR_DATE=$(date +%s) yq -i \'.spec.template.metadata.annotations."reconcile.fluxcd.io/requestedAt"=strenv(CURR_DATE)\' ${worker}.yaml'
                  }
                sh 'git add ${worker}.yaml'
                sh 'git commit -m "Restart ${serviceName}-${worker}. By: ${BUILD_URL}"'
                sh 'git log -1'
                sh 'git push origin master'
              }
            }
          }
        }
      }
    }
  }
}
