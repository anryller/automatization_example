#!/bin/bash

set -xeuo pipefail

#DB_SOURCE_HOST='198.199.125.125:5432'
DB_SOURCE_PASSWORD='123456'
DB_SOURCE_PASSWORD_REWARD='R5MLjFdN'

DB_TARGET_HOST='some-url.eu-central-1.rds.amazonaws.com'
DB_TARGET_PASSWORD='123456'

TARGET_ENV_ARRAY=($(echo $TARGET_ENV | tr "," " "))
readonly TARGET_ENV_ARRAY

DATABASES_ARRAY=($(echo $DATABASES | tr "," " "))
readonly DATABASES_ARRAY

readonly SOURCE_ENV

START_TIME=$SECONDS
for env in "${TARGET_ENV_ARRAY[@]}"; do
    export DB_SOURCE_HOST='some-url.eu-central-1.rds.amazonaws.com'
  for db in "${DATABASES_ARRAY[@]}"; do
    ##Add kostyls
    #delete "loan_" from db name and split it to username
    DB_SOURCE_USERNAME="$(echo ${db}|sed 's/loan_//')_user"
    if [[ $DB_SOURCE_USERNAME == "provider_ts_user" ]]; then
      DB_SOURCE_USERNAME=provider_user
    fi
    DB_SOURCE_DATABASE="${SOURCE_ENV}_${db}"

    DB_TARGET_USERNAME="$(echo ${db}|sed 's/loan_//')_user"
    if [[ $DB_TARGET_USERNAME == "provider_ts_user" ]]; then
      DB_TARGET_USERNAME=provider_user
    fi
    DB_TARGET_DATABASE="${env}_${db}"

    if [[ $DB_TARGET_USERNAME == "reward_user" ]]; then
      if [[ $(echo "$OLD_DB_CONTOURS" | grep "$SOURCE_ENV") ]] || [[ $(echo "$SOURCE_ENV" | grep "core_uat") ]]; then
        DB_SOURCE_PASSWORD=$DB_SOURCE_PASSWORD_REWARD
      fi
    fi

    echo
    echo "--- Environment: $env"
    echo "-- Source database: ${DB_SOURCE_DATABASE}, Source username: ${DB_SOURCE_USERNAME}"
    echo "-- Target database: ${DB_TARGET_DATABASE}, Target username: ${DB_TARGET_USERNAME}"


    echo '-- Drop established to target database connections'
    psql --quiet postgres://postgres:${PGPASSWORD}@${DB_TARGET_HOST}/postgres << END_OF_SCRIPT
      select pg_terminate_backend(pg_stat_activity.pid) from pg_stat_activity where pg_stat_activity.datname = '$DB_TARGET_DATABASE' and pg_stat_activity.pid <> pg_backend_pid();
END_OF_SCRIPT


    echo "-- Recreate target database ${DB_TARGET_DATABASE}"
    psql --quiet postgres://postgres:${PGPASSWORD}@${DB_TARGET_HOST}/postgres << END_OF_SCRIPT
      DROP DATABASE IF EXISTS ${DB_TARGET_DATABASE};
      CREATE DATABASE ${DB_TARGET_DATABASE};
      ALTER DATABASE ${DB_TARGET_DATABASE} OWNER TO ${DB_TARGET_USERNAME};
      GRANT ALL PRIVILEGES ON DATABASE ${DB_TARGET_DATABASE} TO ${DB_TARGET_USERNAME};
      GRANT CONNECT ON DATABASE ${DB_TARGET_DATABASE} TO ${DB_TARGET_USERNAME};
END_OF_SCRIPT

    echo "-- Temporary grant 'rds_superuser' permissions to ${DB_TARGET_USERNAME} user to allow setup extensions."
    psql --quiet postgres://postgres:${PGPASSWORD}@${DB_TARGET_HOST}/postgres << END_OF_SCRIPT
      GRANT rds_superuser TO ${DB_TARGET_USERNAME};
END_OF_SCRIPT


    if [[ -f "${DB_SOURCE_DATABASE}.dump" ]]; then
      echo "-- Found existing dump "${DB_SOURCE_DATABASE}.dump". Skipping creating dump."
      if [[ ! -s "${DB_SOURCE_DATABASE}.dump" ]]; then
        echo '-- Dump file is empty. Please remove the empty dump and try again.'
        exit 1
      fi
    else
      echo "-- Creating ${DB_SOURCE_DATABASE}.dump from Source database"
      pg_dump \
        --dbname postgres://${DB_SOURCE_USERNAME}:${DB_SOURCE_PASSWORD}@${DB_SOURCE_HOST}/${DB_SOURCE_DATABASE} \
        --no-owner \
        --no-privileges \
        --clean \
        -Fc \
        -f "${DB_SOURCE_DATABASE}.dump"
      fi

    echo "-- Restoring ${DB_SOURCE_DATABASE}.dump to Target database ${DB_TARGET_DATABASE}."
    pg_restore \
      --dbname postgres://${DB_TARGET_USERNAME}:${DB_TARGET_PASSWORD}@${DB_TARGET_HOST}/${DB_TARGET_DATABASE} \
      --no-owner \
      "${DB_SOURCE_DATABASE}.dump"

    echo '-- Ensure permissions'
    psql --quiet postgres://postgres:${PGPASSWORD}@${DB_TARGET_HOST}/postgres << END_OF_SCRIPT
      ALTER DATABASE ${DB_TARGET_DATABASE} OWNER TO ${DB_TARGET_USERNAME};
      GRANT ALL PRIVILEGES ON DATABASE ${DB_TARGET_DATABASE} TO ${DB_TARGET_USERNAME};
      GRANT CONNECT ON DATABASE ${DB_TARGET_DATABASE} TO ${DB_TARGET_USERNAME};
      REVOKE rds_superuser FROM ${DB_TARGET_USERNAME};
END_OF_SCRIPT

    echo '-- Add extensions'
    psql --quiet postgres://postgres:${PGPASSWORD}@${DB_TARGET_HOST}/${DB_TARGET_DATABASE} << END_OF_SCRIPT
      CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
END_OF_SCRIPT

  done
done

echo
elapsed=$(( SECONDS - START_TIME ))
eval "echo Elapsed time: $(date -ud "@$elapsed" +'%H hr %M min %S sec')"
