#!/bin/bash

set -xeuo pipefail

#DB_TARGET_HOST= will taken from env
#DB_TARGET_USERNAME= will taken from env
#DB_TARGET_PASSWORD= will taken from env

TARGET_ENV_ARRAY=($(echo $TARGET_ENV | tr "," " "))
readonly TARGET_ENV_ARRAY

DATABASES_ARRAY=($(echo $DATABASES | tr "," " "))
readonly DATABASES_ARRAY

START_TIME=$SECONDS
for env in "${TARGET_ENV_ARRAY[@]}"; do
  for db in "${DATABASES_ARRAY[@]}"; do

      DB_SOURCE_DATABASE="${db}"
    
    ##Set envs
    if [[ $DB_TARGET_HOST == "some-url.eu-central-1.rds.amazonaws.com" ]]; then
    ##IF target db in AWS:
      #delete "loan_" from db name and split it to username
      DB_TARGET_USERNAME="$(echo ${db}|sed 's/loan_//')_user"

      #fix username for provider's db      
      if [[ $DB_TARGET_USERNAME == "provider_ts_user" ]]; then
        DB_TARGET_USERNAME=provider_user
      fi

      #set postgres user
      PG_ADMIN_USERNAME="postgres"

      #split env and db for create dbname
      DB_TARGET_DATABASE="${env}_${db}"
    else
    ##IF target db in Azure:
      #for Azure's dbs, set dbname as is
      DB_TARGET_DATABASE="${db}"
      #fix azure postgres user - replace @ in name to %40 for correct connecting through psql
      PG_ADMIN_USERNAME=($(echo $DB_TARGET_USERNAME | sed "s/\@/\%40/"))
      #fix username for "OWNER" operations - cut '@servername' part
      DB_TARGET_USERNAME=($(echo $DB_TARGET_USERNAME | cut -d@ -f 1))
    fi

    echo
    echo "--- Environment: $env"
    echo "-- Target database: ${DB_TARGET_DATABASE}, Target username: ${DB_TARGET_USERNAME}"

    ##Checking dumpfile exist and non-zero leight
    if [[ -f "dumps/${DB_SOURCE_DATABASE}.dump" ]]; then
      echo "-- Found existing dump dumps/${DB_SOURCE_DATABASE}.dump."
      if [[ ! -s "dumps/${DB_SOURCE_DATABASE}.dump" ]]; then
        echo '-- Dump file is empty. Please remove the empty dump and try again.' && echo "Error! Dump file is empty. Please remove the empty dump and try again ${DB_SOURCE_DATABASE}.dump" >> erorrs_db.txt
        exit 1
      fi

      ##Start restoring backup from file
      echo '-- Drop established to target database connections'
      psql --quiet postgres://${PG_ADMIN_USERNAME}:${PGPASSWORD}@${DB_TARGET_HOST}/postgres << END_OF_SCRIPT
        select pg_terminate_backend(pg_stat_activity.pid) from pg_stat_activity where pg_stat_activity.datname = '$DB_TARGET_DATABASE' and pg_stat_activity.pid <> pg_backend_pid();
END_OF_SCRIPT

      echo "-- Recreate target database ${DB_TARGET_DATABASE}"
      psql --quiet postgres://${PG_ADMIN_USERNAME}:${PGPASSWORD}@${DB_TARGET_HOST}/postgres << END_OF_SCRIPT
        DROP DATABASE IF EXISTS "${DB_TARGET_DATABASE}";
        CREATE DATABASE "${DB_TARGET_DATABASE}";
        ALTER DATABASE "${DB_TARGET_DATABASE}" OWNER TO ${DB_TARGET_USERNAME};
        GRANT ALL PRIVILEGES ON DATABASE "${DB_TARGET_DATABASE}" TO ${DB_TARGET_USERNAME};
        GRANT CONNECT ON DATABASE "${DB_TARGET_DATABASE}" TO ${DB_TARGET_USERNAME};
END_OF_SCRIPT

      if [[ $DB_TARGET_HOST == "some-url.eu-central-1.rds.amazonaws.com" ]]; then
        echo "-- Temporary grant 'rds_superuser' permissions to ${DB_TARGET_USERNAME} user to allow setup extensions."
        psql --quiet postgres://${PG_ADMIN_USERNAME}:${PGPASSWORD}@${DB_TARGET_HOST}/postgres << END_OF_SCRIPT
          GRANT rds_superuser TO ${DB_TARGET_USERNAME};
END_OF_SCRIPT
      fi

      echo "-- Restoring dumps/${DB_SOURCE_DATABASE}.dump to Target database ${DB_TARGET_DATABASE}."
      pg_restore \
        --dbname postgres://${PG_ADMIN_USERNAME}:${PGPASSWORD}@${DB_TARGET_HOST}/"${DB_TARGET_DATABASE}" \
        "dumps/${DB_SOURCE_DATABASE}.dump"

      echo '-- Ensure permissions'
      psql --quiet postgres://${PG_ADMIN_USERNAME}:${PGPASSWORD}@${DB_TARGET_HOST}/postgres << END_OF_SCRIPT
        ALTER DATABASE "${DB_TARGET_DATABASE}" OWNER TO ${DB_TARGET_USERNAME};
        GRANT ALL PRIVILEGES ON DATABASE "${DB_TARGET_DATABASE}" TO ${DB_TARGET_USERNAME};
        GRANT CONNECT ON DATABASE "${DB_TARGET_DATABASE}" TO ${DB_TARGET_USERNAME};
        REVOKE rds_superuser FROM ${DB_TARGET_USERNAME};
END_OF_SCRIPT

      if [[ $DB_TARGET_HOST == "some-url.eu-central-1.rds.amazonaws.com" ]]; then
        echo '-- Add extensions'
        psql --quiet postgres://postgres:${PGPASSWORD}@${DB_TARGET_HOST}/${DB_TARGET_DATABASE} << END_OF_SCRIPT
          CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
END_OF_SCRIPT
      fi
    else
      echo "-- Dump "dumps/${DB_SOURCE_DATABASE}.dump" is not found. Skiping..." && echo "Error! Dump "dumps/${DB_SOURCE_DATABASE}.dump" is not found." >> erorrs_db.txt
    fi

  done
done

echo
elapsed=$(( SECONDS - START_TIME ))
eval "echo Elapsed time: $(date -ud "@$elapsed" +'%H hr %M min %S sec')"
