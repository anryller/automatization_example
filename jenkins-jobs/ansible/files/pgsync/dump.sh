#!/bin/bash

set -xeuo pipefail

#DB_SOURCE_HOST= will take from env
#DB_SOURCE_PASSWORD= will take from env

DATABASES_ARRAY=($(echo $DATABASES | tr "," " "))
readonly DATABASES_ARRAY

readonly SOURCE_ENV

mkdir dumps || true
START_TIME=$SECONDS
for db in "${DATABASES_ARRAY[@]}"; do
  ##Add kostyls
  #delete "loan_" from db name and split it to username
  if [[ $DB_SOURCE_HOST == "some-url.eu-central-1.rds.amazonaws.com" ]]; then
    DB_SOURCE_USERNAME="$(echo ${db}|sed 's/loan_//')_user"
    if [[ $DB_SOURCE_USERNAME == "provider_ts_user" ]]; then
      DB_SOURCE_USERNAME=provider_user
    fi
    DB_SOURCE_DATABASE="${SOURCE_ENV}_${db}"
  else
    DB_SOURCE_DATABASE="${db}"
    #fix azure postgres user - replace @ in name to %40 for correct working pg_dump
    DB_SOURCE_USERNAME=($(echo $DB_SOURCE_USERNAME | sed "s/\@/\%40/"))
  fi
  DUMP_NAME="${db}"

  echo
  echo "--- Environment: $db"
  echo "-- Source database: ${DB_SOURCE_DATABASE}, Source username: ${DB_SOURCE_USERNAME}"

  if [[ -f "${DUMP_NAME}.dump" ]]; then
    echo "-- Found existing dump "${DB_SOURCE_DATABASE}.dump". Skipping creating dump."
    if [[ ! -s "${DUMP_NAME}.dump" ]]; then
      echo '-- Dump file is empty. Please remove the empty dump and try again.'
      exit 1
    fi
  else
    echo "-- Creating ${DB_SOURCE_DATABASE}.dump from Source database"
    pg_dump \
      --dbname postgres://${DB_SOURCE_USERNAME}:${DB_SOURCE_PASSWORD}@${DB_SOURCE_HOST}/${DB_SOURCE_DATABASE} \
      --no-owner \
      --no-privileges \
      --clean \
      -Fc \
      -f dumps/"${DUMP_NAME}.dump"

      
    fi

done
ls -la dumps/
echo
elapsed=$(( SECONDS - START_TIME ))
eval "echo Elapsed time: $(date -ud "@$elapsed" +'%H hr %M min %S sec')"
