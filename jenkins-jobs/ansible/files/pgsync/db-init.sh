#!/bin/bash

set -xeuo pipefail

DB_HOST='some-url.eu-central-1.rds.amazonaws.com'
readonly DB_HOST

TARGET_ENV_ARRAY=($(echo $TARGET_ENV | tr "," " "))
readonly TARGET_ENV_ARRAY

DATABASES_ARRAY=($(echo $DATABASES | tr "," " "))
readonly DATABASES_ARRAY

for env in "${TARGET_ENV_ARRAY[@]}"; do
  for db in "${DATABASES_ARRAY[@]}"; do
    echo "--- Environment: $env, Database: ${env}_${db}"

    # Create user if not exists
    psql \
      --host="${DB_HOST}" \
      --username=postgres \
      --command="SELECT 1 FROM pg_user WHERE usename = '${db}_user'" \
    | grep --quiet 1 \
    || psql \
      --host="${DB_HOST}" \
      --username=postgres \
      --command="CREATE USER ${db}_user WITH ENCRYPTED PASSWORD '123456';"

    # Create database if not exists
    psql \
      --host="${DB_HOST}" \
      --username=postgres \
      --command="SELECT 1 FROM pg_database WHERE datname = '${env}_${db}'" \
    | grep --quiet 1 \
    || psql \
      --host="${DB_HOST}" \
      --username=postgres << EOF
    CREATE DATABASE ${env}_${db};
EOF

    # Ensure permissions
    psql \
      --host="${DB_HOST}" \
      --username=postgres << EOF
ALTER DATABASE ${env}_${db} OWNER TO ${db}_user;
GRANT ALL PRIVILEGES ON DATABASE ${env}_${db} TO ${db}_user;
GRANT CONNECT ON DATABASE ${env}_${db} TO ${db}_user;
EOF

  done
done
